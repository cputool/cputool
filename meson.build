project('cputool', 'c',
  version : '1.1.0',
  license : 'GPL-3.0-or-later',
  default_options : ['c_std=c18', 'werror=true'])

# Setup configuration data
conf = configuration_data()
conf.set_quoted('abs_top_builddir', meson.build_root())
conf.set_quoted('abs_top_srcdir', meson.source_root())
conf.set_quoted('PACKAGE', meson.project_name())
conf.set_quoted('PACKAGE_NAME', meson.project_name())
conf.set_quoted('PACKAGE_STRING',  meson.project_name() + ' ' + meson.project_version())
conf.set_quoted('PACKAGE_VERSION', meson.project_version())
conf.set_quoted('VERSION', meson.project_version())

conf.set('_GNU_SOURCE', true)

# Set various paths
if get_option('system')
  prefix = '/usr'
  libdir = prefix / 'lib64'
  if run_command('test', '-d', libdir).returncode() != 0
    libdir = prefix / 'lib'
  endif
  localstatedir = '/var'
  sysconfdir = '/etc'
else
  prefix = get_option('prefix')
  libdir = prefix / get_option('libdir')
  localstatedir = prefix / get_option('localstatedir')
  sysconfdir = prefix / get_option('sysconfdir')
endif

# if --prefix is /usr, don't use /usr/var for localstatedir or /usr/etc for
# sysconfdir as this makes a lot of things break in testing situations
if prefix == '/usr'
  if localstatedir == '/usr/var'
    localstatedir = '/var'
  endif
  if sysconfdir == '/usr/etc'
    sysconfdir = '/etc'
  endif
endif

runstatedir = get_option('runstatedir')
if runstatedir == ''
  runstatedir = localstatedir / 'run'
endif

bindir = prefix / get_option('bindir')
datadir = prefix / get_option('datadir')
includedir = prefix / get_option('includedir')
infodir = prefix / get_option('infodir')
libexecdir = prefix / get_option('libexecdir')
localedir = prefix / get_option('localedir')
mandir = prefix / get_option('mandir')
sbindir = prefix / get_option('sbindir')
sharedstatedir = prefix / get_option('sharedstatedir')

docdir = get_option('docdir')
if docdir == ''
  docdir = datadir / 'doc' / meson.project_name()
endif

confdir = sysconfdir / meson.project_name()
pkgdatadir = datadir / meson.project_name()


# Work out compiler flags
cc = meson.get_compiler('c')

possible_cc_flags = ['-fPIE']
possible_link_flags = ['-pie']


if get_option('buildtype') != 'debug'
  possible_cc_flags += [
    '-ffunction-sections',
    '-fdata-sections',
  ]

  possible_link_flags += '-Wl,--gc-sections'
endif

# Add arguments
add_project_arguments(cc.get_supported_arguments(possible_cc_flags), language : 'c')
add_project_link_arguments(cc.get_supported_link_arguments(possible_link_flags), language : 'c')


# Locate programs we need

bin_path = [
  '/bin',
  '/usr/bin',
  '/usr/local/bin',
]

required_programs = [
]

required_programs_groups = [
  { 'name': 'rst2man', 'prog': [ 'rst2man', 'rst2man.py', 'rst2man-3' ] },
]

foreach name : required_programs
  prog = find_program(name, dirs: bin_path)
  varname = name.underscorify()
  conf.set_quoted(varname.to_upper(), prog.path())
  set_variable('@0@_prog'.format(varname), prog)
endforeach

foreach item : required_programs_groups
  prog = find_program(item.get('prog'), dirs: bin_path)
  varname = item.get('name').underscorify()
  conf.set_quoted(varname.to_upper(), prog.path())
  set_variable('@0@_prog'.format(varname), prog)
endforeach


# Write out config.h
config_h = configure_file(
  output : 'config.h',
  configuration : conf,
)


executable('cputool', 'cputool.c',
    install: true,
    install_dir: bindir,
)

gen_docs = not get_option('docs').disabled()
if gen_docs
  subdir('docs')
endif


# Display summary
misc_summary = {
  'docs': gen_docs,
}
summary(misc_summary, section: 'Miscellaneous', bool_yn: true, list_sep: ' ')

