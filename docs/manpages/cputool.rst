=======
cputool
=======

----------------------------
CPU usage management utility
----------------------------

:Manual section: 8
:Manual group: System Utilities

.. contents::

SYNOPSIS
========

``cputool`` [*OPTION*] [-- *COMMAND*]


DESCRIPTION
===========

CPUTool is a utility which manages CPU usage and system load using SIGSTOP
and SIGCONT. CPUTool is able to "slow down" process CPU usage and manage
load levels caused by processes utilizing CPU and/or IO resources.

A common use for CPUTool is decreasing the CPU load caused by compression
software or backup software where there is no limit on CPU or IO resources
normally imposed, but it can be used to manage almost any process.


OPTIONS
=======

``-h``, ``--help``

Display command line usage then exit.

``-p``, ``--pid`` *PID*

Manage the CPU usage of a specific PID. This is the most efficient use
of CPUTool as it does not have to walk the process tree to look for
forks of children.

``-P``, ``--pid-pgrp`` *PID*

Manage the CPU usage of a specific PID's entire process group. The same can
be achieved by specifying a *COMMAND* which CPUTool will then execute and
manage the process group created by that command.

``-c``, ``--cpu-limit`` *PERCENT*

Specify the maxium CPU the process / process group can use. Expressed as
percentage of total CPU. Eg. 200 is two full CPUs in a multi processor
system. Specify an integer value.

``-l``, ``--load-limit`` *LOAD*

Specify the maxium load the system may experience for the process process
group to continue running. Specifyng a fractional value is possible
(e.g. 3.5).

``-v``, ``--verbose``

Increase the amount of messages printed to stderr.

-vv will additionally show statistical information.

-vvv will addditionally show signals being sent to processes.

``-V``, ``--version``

Output version information and exit.


EXAMPLES
========

Limit the PID 4711 to 75% use of one CPU core:

::

  cputool -p 4711 -c 75

Run rsync for a local backup only when the system load does not exceed 7.5.
See the note for -l when using program that relies on TCP network
connections:

::

  cputool -l 7.5 -- rsync -av /home /backup/`date +%Y-%m-%d`


NOTES
=====
CPUTool should run on all Linux systems. It depends on the /proc
pseudo-filesystem to detect PIDs and their resource usage.

TCP network connections could time out if process execution is suspended for
extended periods of time (while the load is too high). Not all programs
handle this case well.

BUGS
====

Please report all bugs you discover. This should be done via:

#. the project issue tracker

   `https://gitlab.devlabs.linuxassist.net/cputool/cputool/-/issues
   <https://gitlab.devlabs.linuxassist.net/cputool/cputool/-/issues>`_

Alternatively, you may report bugs to your software distributor / vendor.


AUTHORS
=======

Please refer to the AUTHORS file distributed with CPUTool.


COPYRIGHT
=========

Copyright (c) 2012-2021, AllWorldIT., and the authors listed in the
CPUTool AUTHORS file.


LICENSE
=======

``cputool`` is distributed under the terms of the GNU v3+.
This is free software; see the source for copying conditions. There
is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE
